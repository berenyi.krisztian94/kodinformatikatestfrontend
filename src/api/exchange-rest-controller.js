import api from "./index";

export default class ExchangeRestController {
    getSymbols() {
        return api.get('/symbols')
    }

    convert(from, to, amount) {
        return api.get('/convert', {params: {from, to, amount}})
    }
}