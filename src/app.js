import React from 'react';
import ExchangeRestController from "./api/exchange-rest-controller";
import {Button, Card, Form, InputNumber, notification, Select} from "antd";
import 'antd/es/card/style/index.css'
import 'antd/es/input-number/style/index.css'
import 'antd/es/notification/style/index.css'
import 'antd/es/form/style/index.css'
import 'antd/es/select/style/index.css'
import 'antd/es/empty/style/index.css'
import 'antd/es/button/style/index.css'
import './app.css'

const controller = new ExchangeRestController();

export default function App() {
    const [symbols, setSymbols] = React.useState()
    const [result, setResult] = React.useState()

    React.useEffect(() => {
        controller.getSymbols().then(value => setSymbols(value.data)).catch(handleError)
    })

    const onFinish = values => {
        console.log('Success:', values);
        controller.convert(values.from, values.to, values.amount).then(value => setResult(value.data)).catch(handleError)
    };

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };

    const getOptions = () => {
        if (symbols && Array.isArray(symbols)) {
            return symbols.map((value, index) => (
                <Select.Option key={index} value={value}>value</Select.Option>
            ))
        }
    }

    return (
        <Card title="Online valutaváltó">
            <Form
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}>
                <Form.Item label="Összeg" name="amount" rules={[{required: true}]}>
                    <InputNumber style={{width: 256}}/>
                </Form.Item>
                <Form.Item label="Összeg" name="from" rules={[{required: true}]}>
                    <Select style={{width: 256}} children={getOptions()}/>
                </Form.Item>
                <Form.Item label="Összeg" name="to" rules={[{required: true}]}>
                    <Select style={{width: 256}} children={getOptions()}/>
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Mehet
                    </Button>
                </Form.Item>
            </Form>

            Eredmény: <span children={result}/>
        </Card>
    )
}

function handleError(reason) {
    notification.error({
        message: 'ERROR',
        description: String(reason)
    })
}